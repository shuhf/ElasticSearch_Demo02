package com.shf.myes.doc;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User {
    private String name;
    private String sex;
    private Integer age;
}
